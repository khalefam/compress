//the file is from 
///github.com/troydhanson/misc/blob/master/compression/entropy/shlimit.c: 
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <unordered_map> 
/*
 * Calculaty entropy on a stream. Entropy is updated for each symbol.
 * The final entropy is printed when the end of the stream occurs.
 */

void usage(char *prog) {
  fprintf(stderr, "usage: %s [-v] [file]\n", prog);
  exit(-1);
}

std::unordered_map<int, int> counts; 
unsigned total;

int main(int argc, char * argv[]) {
  int opt,verbose=0,i;
  FILE *ifilef=stdin;
  char *ifile=NULL,line[100];
  double p, lp, sum=0;
 
  while ( (opt = getopt(argc, argv, "v+")) != -1) {
    switch (opt) {
      case 'v': verbose++; break;
      default: usage(argv[0]); break;
    }
  }
  if (optind < argc) ifile=argv[optind++];
 
  if (ifile) {
    if ( (ifilef = fopen(ifile,"rb")) == NULL) {
      fprintf(stderr,"can't open %s: %s\n", ifile, strerror(errno));
      exit(-1);
    }
  }
 
  /* accumulate counts of each byte value */
  int f;
  while ( (fread(&f, sizeof(int), 1, ifilef)) == 1) {
    counts[f]++;
    total++;
  }

  /* compute the final entropy. this is -SUM[0,255](p*l(p))
   * where p is the probability of byte value [0..255]
   * and l(p) is the base-2 log of p. Unit is bits per byte.
   */


 for( auto it = counts.begin(); it!= counts.end(); ++it ){
//fprintf(stderr, "%d %d\n", it->first, it->second);   
    p = 1.0*it->second/total;
    lp = log(p)/log(2);
    sum -= p*lp;
  }
FILE *ff=fopen("results","a");
 printf("%.2f bits per value\n", sum);
fprintf(ff,"%s\tE\t%.2f\n", argv[1],sum); 
  printf("This data can be reduced to %.0f%% of its original size,\n", sum/sizeof(int)/8*100);
  printf("from %u bytes to about %u bytes.\n", total*sizeof(int), (unsigned)(total*(sum/8)));
fclose(ff);
}

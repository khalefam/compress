#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#else
#include <unistd.h>
#endif
#include <sys/types.h>
#include <sys/stat.h>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#define STA
#ifdef STA
int *numbers;
int cnt;
#else
extern int *numbers;
extern int cnt;
#endif

using namespace std;
void bitpack (char *target, size_t * target_offset, int encoding_length,
	      int source);
long
GetFileSize (char *filename)
#ifdef 	STA
{
  struct stat stat_buf;
  int rc = stat (filename, &stat_buf);
  return rc == 0 ? stat_buf.st_size : -1;
}
#else
 ;
#endif

     void read (char *filename)
#ifdef 	STA
{
  long sz = GetFileSize (filename);
  numbers = (int *) malloc (sz);
  FILE *f = fopen (filename, "rb");
  fread (numbers, sizeof (int), sz / sizeof (int), f);
  fclose (f);
  cnt = sz / sizeof (int);
}
#else
 ;
#endif

     int compare (const void *a, const void *b)
#ifdef 	STA
{
  return (*(int *) a - *(int *) b);
}
#else
;
#endif

     int unique_count;
     std::vector < int >
       sorted_unique_numbers;
     std::vector < int >
       cnts;


     void
     init ()
{
  qsort (numbers, cnt, sizeof (int), compare);
unique_count = 0;
  int
    prev = numbers[0] - 1;

  for (int i = 0; i < cnt; i++)
    {
      if (numbers[i] == prev)
	{
	  cnts[unique_count - 1]++;
	}
      else
	{
	  unique_count++;
	  cnts.push_back (1);
	  sorted_unique_numbers.push_back (numbers[i]);
	}
      prev = numbers[i];
    }

}

//start, end and bits fro buckets
vector < int >
  sts;
vector < int >
  ends;
vector < int >
  bits;

long
processL (int bucket_bits)
{

  sts.clear ();
  ends.clear ();
  bits.clear ();

  int
    no_of_buckets = 1 << bucket_bits;
  int
    points_per_bucket = ceil (cnt * 1.0 / no_of_buckets);
  long
    size = 0;
  int
    i = 0;			//for uniqe
  for (int j = 0; j < no_of_buckets; j++)
    {
      //add points as long as we still have 
      int
	x = 0;
      sts.push_back (i);
      long
	range = sorted_unique_numbers[i];
      while (x < points_per_bucket && i < unique_count)
	{
	  x += cnts[i];
	  i++;
	}
      ends.push_back (i >= unique_count ? unique_count - 1 : i);
      range =
	sorted_unique_numbers[i >=
			      unique_count ? unique_count - 1 : i] - range +
	1;
      int
	bits_for_bucket = 0;
      if (range > 0)
	bits_for_bucket = ceil (log (range) / log (2));
      bits.push_back (bits_for_bucket);
      //if we are done with all point then break
      size += 32 /*size of the offset of the bucket */  +
	5
	/* for the number of bits for this bucket, actually 5 bits is sufficietn */
	+ (bits_for_bucket + bucket_bits) * x;
      if (i >= unique_count)
	break;
    }
  return size;
}


long
optimize ()
{
  long
    diff = 0;
  for (int i = 0; i < sts.size () - 1; i++)
    {
      if (bits[i] < bits[i + 1])
	{
	  //move from bucket i+1 to bucket i
	  int
	    j = sts[i];
	  long
	    end = sorted_unique_numbers[j] + (1 << bits[i]) - 1;
	  j = sts[i + 1];
	  long
	    x = 0;
	  while (sorted_unique_numbers[j] < end)
	    {
	      x += cnts[j];
	      j++;
	      if (j > unique_count)
		break;
	    }
	  int
	    o = ends[i];
	  ends[i] = j;
	  sts[i + 1] = j;
	  diff += (bits[i + 1] - bits[i]) * x;
	}
      else if (bits[i] > bits[i + 1])
	{
	  //move from buckt i+1 to i
	}
    }
  return diff;
}

long
process (int bucket_bits)
{
  //now divide the numbers into buckets
  int
    no_of_buckets = 1 << bucket_bits;
  int
    points_per_bucket = ceil (cnt * 1.0 / no_of_buckets);
  long
    size = 0;
  int
    i = 0;
  for (int j = 0; j < no_of_buckets; j++)
    {
      int
	st = i;
      int
	end = i + points_per_bucket;

      end = end > unique_count ? unique_count : end;
      if (st == end)
	break;
      int
	range =
	sorted_unique_numbers[end - 1] - sorted_unique_numbers[st] + 1;

      int
	bits_for_bucket = 0;
      if (range > 0)
	bits_for_bucket = ceil (log (range) / log (2));

      int
	freq = 0;
      for (int l = st; l < end; l++)
	{
	  freq += cnts[l];
	}
      size += (bits_for_bucket + bucket_bits) * freq + 32 + 5;
      i = end;
    }
  size += 32 + 5;
  return size;
}


long
writeHeader (int bucket_bits, FILE * f)
{
  //calcaulate the bits needed
  long
    size = 0;
  char
    max_b = 0;
  int
    min_delta = LONG_MAX;
  int
    max_delta = LONG_MIN;
  int
    no_of_buckets = 1 << bucket_bits;
  int
    tmp = sts.size ();		//no of buckets
  fwrite (&tmp, sizeof (int), 1, f);
  size += 32;
  //write the first point
  tmp = sorted_unique_numbers[sts[0]];
  fwrite (&tmp, sizeof (int), 1, f);
  size += 32;
  for (int i = 0; i < sts.size (); i++)
    {
      max_b = max_b > bits[i] ? max_b : bits[i];
    }
  for (int i = 0; i < sts.size () - 1; i++)
    {
      long
	delta =
	sorted_unique_numbers[sts[i + 1]] - sorted_unique_numbers[sts[i]];
      max_delta = max_delta > delta ? max_delta : delta;
      min_delta = min_delta < delta ? min_delta : delta;
    }
  //write bits for delta
  char
    bit_4_delta = ceil (log (max_delta - min_delta + 1) / log (2));
  fwrite (&bit_4_delta, sizeof (char), 1, f);
  fwrite (&bit_4_delta, sizeof (char), 1, f);
  size += 8 + 8;
  long
    sz = (bit_4_delta + max_b) * sts.size ();
  char *
    bf = (char *) malloc ((sizeof (char) * sz / 8) + 1);
  memset (bf, 0, sz / 8 + 1);
  size_t
    bf_off = 0;
  int
    v = bits[0];
  bitpack (bf, &bf_off, max_b, v);
  for (int i = 1; i < sts.size (); i++)
    {
      v = bits[i];
      bitpack (bf, &bf_off, max_b, v);
      v = sorted_unique_numbers[sts[i]] - sorted_unique_numbers[sts[i - 1]];
      bitpack (bf, &bf_off, bit_4_delta, v);

    }
  size += bf_off;

  return size;
}

#define BL 8

void
bitpack (char *target, size_t * target_offset, int encoding_length,
	 int source)
{
  int
    j;
  for (j = encoding_length - 1; j >= 0; j--, (*target_offset)++)
    {
      target[(*target_offset) / BL] |=
	((source >> j) & 1) << ((BL - (*target_offset) % BL) - 1);
    }
}

int
bitunpack (char *source, size_t * source_offset, int encoding_length)
{
  int
    j,
    target = 0;
  for (j = encoding_length - 1; j >= 0; j--, (*source_offset)++)
    {
      target |=
	(1 &
	 (source[(*source_offset) / BL] >>
	  ((BL - (*source_offset) % BL) - 1))) << j;
    }
  return target;
}

long
compress (int b, size_t size, char *outfile)
{
  long
    sz = 0;
  FILE *
    out = fopen (outfile, "wb");
  processL (b);
  //long o=optimize();
  //fprintf(stderr, "saved %lu\n", o);
  sz += writeHeader (b, out);

  int *
    start_of_buckets;

  int
    no_of_buckets = sts.size ();
  start_of_buckets = (int *) malloc (sizeof (int) * (sts.size () + 1));
  for (int i = 0; i < no_of_buckets; i++)
    start_of_buckets[i] = sorted_unique_numbers[sts[i]];
  start_of_buckets[no_of_buckets] =
    sorted_unique_numbers[ends[no_of_buckets - 1]];

  char *
    bf = (char *) malloc (size * (sizeof (char) + 1));
  size_t
    bf_off = 0;
  for (int i = 0; i < cnt; i++)
    {
      //determine bucket
      int
	num = numbers[i];
      int *
	low =
	std::upper_bound (start_of_buckets, start_of_buckets + no_of_buckets,
			  num);
      int
	index;
      index = low - 1 - start_of_buckets;
      if (index < 0)
	continue;
      if (bf_off > (size) * 8)
	break;
      bitpack (bf, &bf_off, b, index);
      int
	off = num - start_of_buckets[index];
      bitpack (bf, &bf_off, bits[index], off);

    }
  sz += bf_off;

  if (bf_off % 8 != 0)
    bf_off = bf_off / 8 + 1;
  else
    bf_off = bf_off / 8;

  fwrite (bf, sizeof (char), bf_off, out);

  fclose (out);
  return sz;
}


int
main (int argc, char *argv[])
{
  int
    increasing = 0;
  long
    prev = ULLONG_MAX;

#ifdef _MSC_VER
  char *
    filename = "c:/data/art2-random-delta.bat";
#else
  char *
    filename = argv[1];
#endif
  int
    indx = -2;
  
 if (argc == 3)
    {
 indx = atoi (argv[2]);
  
    }
 read (filename);
  init ();
 unsigned long
    min = ULLONG_MAX;
  int
    min_i = -1;
#ifdef TOO_VERBOSE
 FILE *
    f = fopen ("hist_details", "a");
#endif
  if (indx > 0)
    {
      long
	p = processL (indx);
//      p -= optimize ();
      min = p;
      min_i = indx;
    }
  else
    for (int i = 1; i < 31; i++)
      {
	long
	  p = processL (i);
//	p -= optimize ();
#ifdef TOO_VERBOSE
	fprintf (f, "%s\tHL\t%lu\t@%d\n", filename, p, i);
#endif
	if (min > p)
	  {
	    min = p;
	    min_i = i;
	  }

	if (prev < p && increasing)
	  {
	    break;
	  }
	if (prev < p)
	  {
	    increasing = 1;
	  }
	else
	  increasing = 0;
	prev = p;

      }

#ifdef TOO_VERBOSE
  fclose (f);
#endif
  free (numbers);
  read (filename);
  long
    smin = min;
  char
    outfilename[100];
  sprintf (outfilename, "%s.histo.9", filename);
  //compress (cnt *4, min_i, "out");
  compress (min_i, min / 8, outfilename);
  long
    sz = GetFileSize (outfilename);
  FILE *
    ff = fopen ("results", "a");
  fprintf (ff, "%s\tHC\t%lu\t%lu\t@%d\n", filename, smin, sz * 8, min_i);
  fprintf (stdout, "%s\tHC\t%lu\t%lu\t@%d\n", filename, smin, sz * 8, min_i);

  fclose (ff);
  return 0;
}

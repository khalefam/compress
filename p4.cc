#include<limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <algorithm>
#include <stdio.h>      
#include <stdlib.h>     
#include <time.h>
#include <math.h>
#include <vector>
int *numbers;
int *sorted_numbers;
int cnt;
using namespace std;


long GetFileSize(char* filename)
{
    struct stat stat_buf;
    int rc = stat(filename, &stat_buf);
    return rc == 0 ? stat_buf.st_size : -1;
}

void read(char* filename){
	long sz=GetFileSize(filename);
	numbers=(int *) malloc(sz);
	FILE * f=fopen(filename,"rb");
	fread(numbers,sizeof(int), sz/sizeof(int),f);
	fclose(f);	
	cnt=sz/sizeof(int);
}

void writerandomfile(char* filename,int size=100000){
	
	int num;
	FILE *f=fopen(filename,"wb");
	srand(time(NULL));
	for(int i=0;i<size;i++){
		num=rand();
		fwrite(&num,sizeof(int),1,f);
	}
	fclose(f);
}
unsigned long estimatesize(long long from, long long to){
	
	int bits=ceil( log(to+2-from)/log(2));
	
	int* low=std::lower_bound(sorted_numbers, sorted_numbers+cnt-1,from);
	int* high=std::upper_bound(sorted_numbers, sorted_numbers+cnt-1,to);
	int d=high-low;
	unsigned long size=bits*cnt+(cnt-d)*32;
	return size;
}


int compare (const void * a, const void * b)
{
  return ( *(int*)a - *(int*)b );
}

long process(int *min, int *start){
	qsort(numbers,cnt,sizeof(int),compare);
	
	sorted_numbers=numbers;
	numbers=NULL;
	unsigned long min_size_b=ULLONG_MAX;
	int min_s=-1;
	int min_b=-1;
		
	for(int b=1;b<=31;b++){
	//start with b bits
 		int prev_begin=sorted_numbers[0]-1;
		int prev_end=sorted_numbers[0]-1;
		for(int i=0;i<cnt;i++){
			 long long begin=sorted_numbers[i];
			 long long end=sorted_numbers[i]+(unsigned   (1<<b))-1-1;
			if(begin==prev_begin&& end==prev_end) continue;
			unsigned long size=estimatesize(begin,end);
			if(size<min_size_b) {
				min_size_b=size;
				min_s=i;
				min_b=b;
			}
		}	
		printf("bits %d  min size %f [%d,%d]\n",min_b,min_size_b*1.0/cnt, sorted_numbers[min_s], sorted_numbers[min_s]+(1<<min_b)-1);
		
	}
	printf("bits %d  min size %f [%d,%u]\n",min_b,min_size_b*1.0/cnt, sorted_numbers[min_s], sorted_numbers[min_s]+(1<<min_b)-1);
	*min=min_b;
        *start=min_s;
	return	min_size_b*1;
}

//Error	3	error C4996: 'fopen': This function or variable may be unsafe. Consider using fopen_s instead.
//To disable deprecation, use  See online help for details.	c:\users\khalefa\documents\visual studio 2012\projects\bitpack\p4\p4.cpp	26	1	p4

int main(int argc, char *argv[])
{
	char* filename=argv[1];
	read(filename);
	int b;
	int strt;
	long x=process(&b,&strt );
	printf("P %s %lu @%d,%d \n", filename,x,b,strt); 

	FILE *ff=fopen("results","a");
	fprintf(ff,"%s\tP\t%lu\t@%d,%d\n", filename,x,b,strt); 
	fclose(ff);

}
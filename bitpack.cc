#include<stdio.h>
#include <string.h>
#include <stdlib.h>
#include <vector>
std::vector < int >
  exceptions;
void
compress (char *inputfile, char *outputfile, int limit);
void
decompress (char *inputfile, char *outputfile);
int
find_optimal_limit ();

long
file_size (char *filename)
{
  long
    sz;
  FILE *
    fp = fopen (filename, "rb");

  fseek (fp, 0L, SEEK_END);
  sz = ftell (fp);
  fclose (fp);
  return sz;
}

void
report_compression_bits (char * inputfile, char *compressed, int limit, int count)
{
  FILE *
    f = fopen ("results", "a");
  double
    ratio = file_size (compressed);
  ratio /= count;

  fprintf (f, "BP %s %d  %.2f\n", inputfile, limit, ratio);
  fclose (f);
  fprintf (stderr, "BP %s %d  %.2f\n", inputfile, limit, ratio);

}

// it is always good to use argument
void
printUsage ()
{
  printf ("bitpack use exception list for compressing and decompressing\n");
  printf ("To compress use \n bp inputfile outputfile  limit\n");
  printf ("To decompress use \n ubp inputfile outputfile \n");
}

int
main (int argc, char *argv[])
{
  if (strcmp (argv[0], "./bp") == 0)
    {
      if (argc != 4)
	{
	  printUsage ();
	  return 1;
	}
      compress (argv[1], argv[2], atoi (argv[3]));
     // report_compression_ratio (argv[1], argv[2], atoi (argv[3]));
      return 0;
    }
  else if (strcmp (argv[0], "./ubp") == 0)
    {
      if (argc != 3)
	{
	  printUsage ();
	  return 1;
	}
      decompress (argv[1], argv[2]);
      return 0;
    }
  else
    {

      printUsage ();
    }

}

void
setbit (char *bitarry, int bitindex, int value)
{
  if (value == 1)
    *(bitarry + bitindex / 8) |= (1 << (bitindex % 8));
  else
    *(bitarry + bitindex / 8) &= ~(1 << (bitindex % 8));
}

int
getbit (char *bitarry, int bitindex)
{
  return (*(bitarry + bitindex / 8) & (1 << (bitindex % 8))) != 0;
}

int
decode2 (unsigned int *buf, int *st_indx, int limit, unsigned int *data)
{
  static int
    c = 0;
  c++;
  int
    n = (sizeof (int) * 8 - *st_indx) / limit;
  int
    j = 0;
  int
    bit = *st_indx;

  for (j = 0; j < n; j++)
    {
      int
	num = 0;
      for (int i = 0; i < limit; i++)
	{
	  num += (1 << i) * getbit ((char *) buf, bit);
	  bit++;
	}
      data[j] = num;
    }
  if ((sizeof (int) * 8 - *st_indx) % limit != 0)
    {
      int
	num = 0;
      int
	i = 0;
      for (; bit < sizeof (int) * 8; i++)
	{
	  num += (1 << i) * getbit ((char *) buf, bit);
	  bit++;
	}
      bit = 0;
      unsigned int *
	p = (buf + 1);
      for (; i < limit; i++)
	{
	  num += (1 << i) * getbit ((char *) p, bit);
	  bit++;
	}
      *st_indx = bit;
      data[j] = num;
      return 0;
    }
  *st_indx = bit % (sizeof (int) * 8);
  return 1;
}

int
decode (unsigned int *buf, int *st_indx, int limit, unsigned int *data)
{
  int
    num = 0;
  int
    j = 0;
  int
    v = 1;
  int
    k = 0;
  int
    s = *st_indx;
  int
    not_done = 0;
  int
    i = 0;
  for (i = s; i < sizeof (int) * 8; i++)
    {
      not_done = 1;
      num += v * getbit ((char *) buf, i);
      v = v * 2;
      if ((1 + i + s) % (limit) == 0 && i > s)
	{
	  data[j++] = num;
	  num = 0;
	  v = 1;
	  not_done = 0;
	}
    }
  if (not_done)
    {
      buf++;
      *st_indx = 0;
      for (k = i;; k++)
	{
	  (*st_indx)++;
	  num += v * getbit ((char *) buf, k - 32);
	  v = v * 2;
	  if ((1 + k + s) % (limit) == 0)
	    break;
	}
      data[j++] = num;

    }

  return 0;
}

//we pass limit and limit number to reduce the computation a litte


int
encode (unsigned int *buf, int *buf_indx, int number,
	int limit_number, int limit, int *ret)
{
  int
    i = 0;
  int
    val;
  int
    overflow = 0;
  if (number >= limit_number)
    {
      //it is exception
      exceptions.push_back (number);
      number = limit_number;
    }
  //encode number in binary
  for (i = 0; i < limit; i++)
    {
      val = number % 2;
      setbit ((char *) buf, *buf_indx, val);
      number = number / 2;
      *buf_indx = (*buf_indx) + 1;
      if (*buf_indx == sizeof (int) * 8)
	{
	  *ret = *buf;
	  overflow = 1;
	  *buf = 0;
	  *buf_indx = 0;
	}

    }

  return overflow;
}

// here we 
//sa 
// 10 2
//unsigned int encode(unsigned int number, int limit, int bitindx){
//      return number 
//}
void
compress (char *inputfile, char *outputfile, int limit)
{
  FILE *
    fin;
  FILE *
    fout;
  int
    exception_list_start = 0;	//it is not known now
  int
    size = 0;
  int
    number;
  unsigned int
    max_number = (1 << limit) - 1;
  unsigned int
    buf = 0;
  int
    tmp;
  int
    indx;
  fin = fopen (inputfile, "rb");
  fout = fopen (outputfile, "wb");
  fwrite (&limit, sizeof (int), 1, fout);
//we need to uodate this late
  fwrite (&exception_list_start, sizeof (int), 1, fout);

  indx = 0;
  for (size = 0; fread (&number, sizeof (int), 1, fin) == 1; size++)
    {
      int
	o = encode (&buf, &indx, number, max_number, limit, &tmp);
      if (o == 1)
	fwrite (&tmp, sizeof (int), 1, fout);
    }
  fwrite (&buf, sizeof (int), 1, fout);

  long
    pos = ftell (fout);

  //write exceptions
  for (int i = 0; i < exceptions.size (); i++)
    {
      int
	t = exceptions[i];
      fwrite (&t, sizeof (int), 1, fout);
    }
  fseek (fout, sizeof (int), SEEK_SET);
  exception_list_start = pos;
  fwrite (&exception_list_start, sizeof (int), 1, fout);

  fclose (fin);
  fclose (fout);
report_compression_bits(inputfile, outputfile, limit, size); 
}

void
decompress (char *inputfile, char *outputfile)
{

  FILE *
    in = fopen (inputfile, "rb");
  int
    limit = 0;
  int
    offset;
  int
    st = 0;
  unsigned int *
    data;
  unsigned int
    buf[2];
  fread (&limit, sizeof (int), 1, in);
  int
    limitmax = (1 >> (limit + 1)) - 1;
  fread (&offset, sizeof (int), 1, in);
  int
    data_len = sizeof (int) * 8 / limit + 1;
  data = new unsigned int[data_len];
  fread (buf, sizeof (int), 2, in);
  int
    end_of_stream = 0;
  FILE *
    out = fopen ("out.t", "wt");
  for (;;)
    {

      int
	l = decode2 (buf, &st, limit, data);
      for (int i = 0; i < data_len - l; i++)
	{

	  fprintf (out, "%d\n", data[i]);
	}
      int
	b = buf[1];
      if (end_of_stream)
	break;
      int
	x;
      int
	c = fread (&x, sizeof (int), 1, in);
      if (c == 0)
	end_of_stream = 1;
      buf[0] = b;
      buf[1] = x;
    }
  fclose (in);
  fclose (out);
}

void
write ()
{
  FILE *
    f = fopen ("a.txt", "wb");
  FILE *
    o = fopen ("in.t", "wt");
  int
    i = 4;
  fwrite (&i, sizeof (int), 1, f);
  fprintf (o, "%d\n", i);
  i = 8;
  fwrite (&i, sizeof (int), 1, f);
  fprintf (o, "%d\n", i);
  for (i = 0; i < 1000; i++)
    {
      int
	m = i % 9;
      fwrite (&m, sizeof (int), 1, f);
      fprintf (o, "%d\n", m);
    }
  fclose (f);
  fclose (o);
}

-- select me.s, mh.s/mn.s, mhi.s/mn.s , mp.s/mn.s, mr.s
use expr;
drop table m;
create table m (fname varchar(1000),type varchar(40), s float , other varchar(1000),delta int);
select me.fname,  me.s as E, mh.s/mn.s as Huffman, mhi.s/mn.s as His, mp.s/mn.s as P4, mr.s as R ,
 me1.s as delta_E, mh1.s/mn1.s as delta_Huffman, mhi1.s/mn1.s as delta_His, mp1.s/mn1.s as P4delta, mr1.s as R 
from   m  me, m mn,     m mh, m mhi  , m mp, m mr, 
       m me1, m mn1, m mh1, m mhi1  , m mp1, m mr1
where mh.fname=me.fname and mhi.fname=me.fname and mn.fname=me.fname  and mp.fname=me.fname and mr.fname=me.fname and
       mh1.fname=me.fname and mhi1.fname=me.fname and mn1.fname=me.fname  and mp1.fname=me.fname and mr1.fname=me.fname and me1.fname=me.fname
 and

me.type='E' and mh.type='HUFF' and mhi.type= 'HL' and mn.type='N' and mp.type='P' and mr.type='R' and
me1.type='E' and mh1.type='HUFF' and mhi1.type= 'HL' and mn1.type='N' and mp1.type='P' and mr1.type='R'

and mn.delta=0 and me.delta=0 and mh.delta=0 and mp.delta=0 and mr.delta=0 and mhi.delta=0
and mn1.delta=1 and me1.delta=1 and mh1.delta=1 and mp1.delta=1 and mr1.delta=1 and mhi1.delta=1
;


select * from (
select me.fname fname,  me.s as E, mh.s/mn.s as Huffman, mhi.s/mn.s as His, mp.s/mn.s as P4, mr.s as R , mx.s/mn.s as XZ,
 me1.s as delta_E, mh1.s/mn1.s as delta_Huffman, mhi1.s/mn1.s as delta_His, mp1.s/mn1.s as P4delta, mr1.s as R1, mx1.s/mn1.s as XZ1,  mn.s N 
from   m  me, m mn,     m mh, m mhi  , m mp, m mr, m mx,
       m me1, m mn1, m mh1, m mhi1  , m mp1, m mr1, m mx1
where mh.fname=me.fname and mhi.fname=me.fname and mn.fname=me.fname  and mp.fname=me.fname and mr.fname=me.fname and mx.fname=me.fname and
       mh1.fname=me.fname and mhi1.fname=me.fname and mn1.fname=me.fname  and mp1.fname=me.fname and mr1.fname=me.fname and me1.fname=me.fname and mx1.fname=me.fname
 and

me.type='E' and mh.type='HUFF' and mhi.type= 'HL' and mn.type='N' and mp.type='P' and mr.type='R' and mx.type='XZ' and
me1.type='E' and mh1.type='HUFF' and mhi1.type= 'HL' and mn1.type='N' and mp1.type='P' and mr1.type='R' and mx1.type='XZ' 

and mn.delta=0 and me.delta=0 and mh.delta=0 and mp.delta=0 and mr.delta=0 and mhi.delta=0 and mx.delta=0 
and mn1.delta=1 and me1.delta=1 and mh1.delta=1 and mp1.delta=1 and mr1.delta=1 and mhi1.delta=1 and mx1.delta=1
order by fname
) x
;

#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <algorithm>
#include <stdio.h>      
#include <stdlib.h>     
#include <time.h>
#include <math.h>
#include <vector>
int *numbers;
int cnt;

long GetFileSize(char* filename)
{
    struct stat stat_buf;
    int rc = stat(filename, &stat_buf);
    return rc == 0 ? stat_buf.st_size : -1;
}


void read(char* filename){
	long sz=GetFileSize(filename);
	numbers=(int *) malloc(sz);
	FILE * f=fopen(filename,"rb");
	fread(numbers,sizeof(int), sz/sizeof(int),f);
	fclose(f);	
	cnt=sz/sizeof(int);
}


float process(){
	double min=INT_MAX;
	double max=INT_MIN;
	for(int i=0;i<cnt;i++){
		if(numbers[i]>max)max=numbers[i];
		if(numbers[i]<min)min=numbers[i];
	}
	return (log(max-min)/log(2));
}


int main(int argc, char *argv[])
{

	read(argv[1]);
	float x=process();
	
	printf("R %s %.2f  \n", argv[1],x); 
	FILE *ff=fopen("results","a");
	fprintf(ff,"%s\tR\t%.2f\n", argv[1],x); 
	fprintf(ff,"%s\tN\t%d\n", argv[1],cnt); 
	fclose(ff);
	
}

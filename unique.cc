#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#else
#include <unistd.h>
#endif
#include <sys/types.h>
#include <sys/stat.h>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <limits.h>
#include <string.h>
#define STA
#ifdef STA
int *numbers;
int cnt;
#else
extern int *numbers;
extern int cnt;
#endif
int divisor=1;

using namespace std;
long
GetFileSize (char *filename)
#ifdef 	STA
{
  struct stat stat_buf;
  int rc = stat (filename, &stat_buf);
  return rc == 0 ? stat_buf.st_size : -1;
}
#else
 ;
#endif

     void read (char *filename)
{
  long sz = GetFileSize (filename);
  long i;
  numbers = (int *) malloc (sz);
  FILE *f = fopen (filename, "rb");
  fread (numbers, sizeof (int), sz / sizeof (int), f);
  fclose (f);
  cnt = sz / sizeof (int);
  for(i=0;i<cnt;i++)
       numbers[i]=numbers[i]/divisor;
} 

     int compare (const void *a, const void *b)
#ifdef 	STA
{
  return (*(int *) a - *(int *) b);
}
#else
;
#endif

     int unique_count;
     std::vector < int >
       sorted_unique_numbers;
     std::vector < int >
       cnts;

     void
     init ()
{

  qsort (numbers, cnt, sizeof (int), compare);
  unique_count = 0;
  int
    prev = numbers[0] - 1;

  for (int i = 0; i < cnt; i++)
    {
      if (numbers[i] == prev)
	{
	  cnts[unique_count - 1]++;
	}
      else
	{
	  unique_count++;
	  cnts.push_back (1);
	  sorted_unique_numbers.push_back (numbers[i]);
//          fprintf(stdout, "%d\n",numbers[i]);
	}
      prev = numbers[i];
    }

}
int
main (int argc, char *argv[])
{
#ifdef _MSC_VER
  char *
    filename = "c:/data/art2-s0.001.bat";
#else
  char *
    filename = argv[1];
#endif
if(argc==3)
divisor=atoi(argv[2]);  
read (filename);
  init ();
 FILE *
    ff = fopen ("results", "a");
  fprintf (ff, "%s\tU\t%lu\t%lu\n", filename, unique_count, cnt);
   fprintf (stdout ,"%s\tU\t%lu\t%lu\n", filename, unique_count, cnt);
  fclose (ff);
  return 0;

}

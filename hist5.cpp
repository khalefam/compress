#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#else
#include <unistd.h>
#endif
#include <sys/types.h>
#include <sys/stat.h>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <limits.h>
#include <string.h>
#define STA
#ifdef STA
int *numbers;
int cnt;
#else
extern int *numbers;
extern int cnt;
#endif

using namespace std;

long
	GetFileSize (char *filename)
#ifdef 	STA
{
	struct stat stat_buf;
	int rc = stat (filename, &stat_buf);
	return rc == 0 ? stat_buf.st_size : -1;
}
#else
	;
#endif

void read (char *filename)
#ifdef 	STA
{
	long sz = GetFileSize (filename);
	numbers = (int *) malloc (sz);
	FILE *f = fopen (filename, "rb");
	fread (numbers, sizeof (int), sz / sizeof (int), f);
	fclose (f);
	cnt = sz / sizeof (int);
}
#else
	;
#endif

int compare (const void *a, const void *b)
#ifdef 	STA
{
	return (*(int *) a - *(int *) b);
}
#else
	;
#endif

int unique_count;
std::vector < int >
	sorted_unique_numbers;
std::vector < int >
	cnts;

void
	init ()
{

	qsort (numbers, cnt, sizeof (int), compare);
	unique_count = 0;
	int
		prev = numbers[0] - 1;

	for (int i = 0; i < cnt; i++)
	{
		if (numbers[i] == prev)
		{
			cnts[unique_count - 1]++;
		}
		else
		{
			unique_count++;
			cnts.push_back (1);
			sorted_unique_numbers.push_back (numbers[i]);
		}
		prev = numbers[i];
	}

}

long processL(int bucket_bits){
	
	int
		no_of_buckets = 1 << bucket_bits;
	int
		points_per_bucket = ceil (cnt * 1.0 / no_of_buckets);
	long
		size = 0;
	int
		i = 0; //for uniqe
	for (int j = 0; j < no_of_buckets; j++)
	{
		//add points as long as we still have 
		int x=0;
		long range=sorted_unique_numbers[i];
		while (x<points_per_bucket&& i<unique_count){
			x+=cnts[i];
			i++;			
		}
		range=sorted_unique_numbers[i>=unique_count?unique_count-1 :i]-range;
		int
			bits_for_bucket = 0;
		if (range > 0)
			bits_for_bucket = ceil (log (range) / log (2));
		//if we are done with all point then break
		size+=32/*size of the offset of the bucket*/+8 /* for the number of bits for this bucket, actually 5 bits is sufficietn*/+ (bits_for_bucket + bucket_bits) * x;
		if(i>=unique_count) break;
	}
	return size;
}
long
	process (int bucket_bits)
{
	//now divide the numbers into buckets
	int
		no_of_buckets = 1 << bucket_bits;
	int
		points_per_bucket = ceil (cnt * 1.0 / no_of_buckets);
	long
		size = 0;
	int
		i = 0;
	for (int j = 0; j < no_of_buckets; j++)
	{
		int
			st = i;
		int
			end = i + points_per_bucket;

		end = end > unique_count ? unique_count : end;
		if (st == end)
			break;
		int
			range =
			sorted_unique_numbers[end - 1] - sorted_unique_numbers[st] + 1;

		int
			bits_for_bucket = 0;
		if (range > 0)
			bits_for_bucket = ceil (log (range) / log (2));

		int
			freq = 0;
		for (int l = st; l < end; l++)
		{
			freq += cnts[l];
		}
		size += (bits_for_bucket + bucket_bits) * freq + 32 + 8;
		i = end;
	}
	size += 32 + 8;
	return size;
}

int *
	start_of_buckets;
char *
	bits_of_buckets;
long
	writeStratandRange (int bucket_bits, FILE * f)
{
	//now divide the numbers into buckets
	long
		size = 0;
	int
		max_b = 0;
	int
		min_delta = LONG_MAX;
	int
		max_delta = LONG_MIN;
	int
		no_of_buckets = 1 << bucket_bits;
	int
		points_per_bucket = ceil (unique_count * 1.0 / no_of_buckets);

	int
		i = 0;
	start_of_buckets = (int *) malloc (sizeof (int) * (no_of_buckets + 1));
	bits_of_buckets = (char *) malloc (sizeof (char) * no_of_buckets);

	int
		prev = 0;
	for (int j = 0; j < no_of_buckets; j++)
	{
		int
			st = i;
		int
			end = i + points_per_bucket;

		end = end > unique_count ? unique_count : end;
		if (st == end)
			break;
		int
			range =
			sorted_unique_numbers[end - 1] - sorted_unique_numbers[st] + 1;
		int
			bits_for_bucket = 0;
		if (range > 0)
			bits_for_bucket = ceil (log (range) / log (2));

		int
			start = sorted_unique_numbers[st];
		char
			b = bits_for_bucket;
		if (b > max_b)
			max_b = b;
		int
			d = start - prev;
		if (d > max_delta)
		{
			max_delta = d;
		}
		else if (d < min_delta)
		{
			min_delta = d;
		}
		prev = start;
		fwrite (&start, sizeof (int), 1, f);
		fwrite (&b, sizeof (char), 1, f);

		start_of_buckets[j] = start;
		bits_of_buckets[j] = b;
		i = end;
	}
	start_of_buckets[no_of_buckets] =
		sorted_unique_numbers[unique_count - 1] + 1;
	int
		s =
		ceil (log (max_delta - min_delta) / log (2)) +
		ceil (log (max_b) / log (2));
	size += s * no_of_buckets;
	return size;
	//fclose(ff);
}
void
	ReadStratandRange (int bucket_bits, FILE * f)
{
	int
		no_of_buckets = 1 << bucket_bits;
	int
		i = 0;
	start_of_buckets = (int *) malloc (sizeof (int) * (no_of_buckets));
	bits_of_buckets = (char *) malloc (sizeof (char) * no_of_buckets);
	for (int j = 0; j < no_of_buckets; j++)
	{
		int
			start = 0;
		char
			b = 0;

		fread (&start, sizeof (int), 1, f);
		fread (&b, sizeof (char), 1, f);

		start_of_buckets[j] = start;
		bits_of_buckets[j] = b;

		//printf("Bucket %d : %d %d\n", j,start, b); 

	}

}

void
	writerandomfile (char *filename)
{
	int
		num;
	FILE *
		f = fopen (filename, "wb");
	int
		nums[] = { 1, 2, 3, 1, 2, 3, 0, 5,
		5, 1, 2, 1
	};
	for (int i = 0; i < sizeof (nums) / sizeof (int); i++)
	{
		num = nums[i];
		fwrite (&num, sizeof (int), 1, f);
	}
	fclose (f);
}

#define BL 8

void
	bitpack (char *target, size_t * target_offset, int encoding_length,
	int source)
{
	int
		j;
	for (j = encoding_length - 1; j >= 0; j--, (*target_offset)++)
	{
		target[(*target_offset) / BL] |=
			((source >> j) & 1) << ((BL - (*target_offset) % BL) - 1);
	}
}

int
	bitunpack (char *source, size_t * source_offset, int encoding_length)
{
	int
		j,
		target = 0;
	for (j = encoding_length - 1; j >= 0; j--, (*source_offset)++)
	{
		target |=
			(1 &
			(source[(*source_offset) / BL] >>
			((BL - (*source_offset) % BL) - 1))) << j;
	}
	return target;
}

long
	compress (long size, int bits, char *outfile)
{
	long
		sz = 0;
	FILE *
		out = fopen (outfile, "wb");
	char
		b = bits;
	fwrite (&cnt, sizeof (int), 1, out);
	sz += 32;
	fwrite (&b, sizeof (char), 1, out);
	sz += 8;
	sz += writeStratandRange (bits, out);
	sorted_unique_numbers.clear ();
	cnts.clear ();
	int
		no_of_buckets = (1 << bits) + 1;

	char *
		bf = (char *) malloc (sizeof (char) * (size + 1));
	memset (bf, 0, size);
	size_t bf_off = 0;
	for (int i = 0; i < cnt; i++)
	{
		//determine bucket
		int
			num = numbers[i];
		int *
			low =
			std::upper_bound (start_of_buckets, start_of_buckets + no_of_buckets,
			num);
		int
			index;
		index = low - 1 - start_of_buckets;
		bitpack (bf, &bf_off, bits, index);
		int
			off = num - start_of_buckets[index];
		bitpack (bf, &bf_off, bits_of_buckets[index], off);
	}
	sz += bf_off;
	if (bf_off % 8 != 0)
		bf_off = bf_off / 8 + 1;
	else
		bf_off = bf_off / 8;
	fwrite (bf, sizeof (char), bf_off, out);

	fclose (out);
	return sz;
}

void
	decompress (char *infile)
{
	long
		sz = GetFileSize (infile);
	FILE *
		in = fopen (infile, "rb");
	int
		count = 0;
	fread (&count, sizeof (int), 1, in);
	char
		b = 0;
	fread (&b, sizeof (char), 1, in);

	int
		bits = b;
	ReadStratandRange (bits, in);
	int
		no_of_buckets = 1 << bits + 1;
	size_t remaing = sz - ftell (in);
	int *
		nums = (int *) malloc (sizeof (int) * count);
	char *
		bf = (char *) malloc (remaing);
	fread (bf, sizeof (char), remaing, in);
	size_t bf_off = 0;
	for (int i = 0; i < count; i++)
	{
		//get bucket
		int
			index = bitunpack (bf, &bf_off, bits);
		int
			off = bitunpack (bf, &bf_off, bits_of_buckets[index]);
		int
			num = start_of_buckets[index] + off;
		//printf("num %d  %d + %d\n",num,index,off);
		/*
		int num=numbers[i];
		int * low=std::upper_bound(start_of_buckets, start_of_buckets+ no_of_buckets,num);
		int index;
		index=low-1-start_of_buckets;
		printf("bucket id for %d is %d\n",num, index);
		bitpack(bf, &bf_off, bits,index );
		int off=num-start_of_buckets[index];
		bitpack(bf, &bf_off, bits_of_buckets[index],off );
		*/
	}

	fclose (in);
}


int
	main (int argc, char *argv[])
{
#ifdef _MSC_VER
	char *
		filename = "c:/data/art2-s0.001.bat";
#else
	char *
		filename = argv[1];
#endif
	read (filename);
	init ();
	unsigned long
		min = ULLONG_MAX;
	int
		min_i = -1;
#ifdef TOO_VERBOSE
	FILE *
		f = fopen ("hist_details", "a");
#endif
	for (int i = 1; i < 31; i++)
	{
		long
			p = processL (i);
#ifdef TOO_VERBOSE
		fprintf (f, "%s\tHL\t%lu\t@%d\n", filename, p, i);
#endif
		if (min > p)
		{
			min = p;
			min_i = i;
		}
	}
	free (numbers);
#ifdef TOO_VERBOSE
	fclose (f);
#endif
	read (filename);
	long
		smin = min;
	compress (min / 8, min_i, "out");
	long sz=GetFileSize("out");
	FILE *
		ff = fopen ("results", "a");
	fprintf (ff, "%s\tHC\t%lu\t%lu\t@%d\n", filename, smin,  sz*8, min_i);
	fclose (ff);
	return 0;
}

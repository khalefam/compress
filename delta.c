#include <stdio.h>
#include <string.h>
#include <stdlib.h>
void
split_name_ext (char *str, char *f, char *ext)
{
  int l = strlen (str);
  int i;
  *ext = '\0';
  *f = '\0';
  for (; l > 0 && str[l] != '.'; l--);
  if (l == 0)
    l = strlen (str);
  for (i = 0; i < strlen (str); i++)
    {
      if (i < l)
	{
	  *f = str[i];
	  f++;
	}
      else if (l < i)
	{
	  *ext = str[i];
	  ext++;
	}
    }
  *ext = '\0';
  *f = '\0';
}

int
delta (char *filename)
{
  int i, size;
  FILE *fp = fopen (filename, "rb");
  char file_name[1000];
  char ext[1000];
  split_name_ext (filename, file_name, ext);
  char outfilename[1000];
  sprintf (outfilename, "%s-delta.%s", file_name, ext);
  FILE *out = fopen (outfilename, "wb");
  int f;
  int prev;
  if (fp != 0)
    {
      fread (&prev, sizeof (int), 1, fp);
      for (i = 0; fread (&f, sizeof (int), 1, fp) == 1; i++)
	{
	  int d = f - prev;
	  prev = f;
	  fwrite (&d, sizeof (int), 1, out);
	}
      fclose (fp);
      fclose (out);
      size = i;
    }
}

int
main (int argc, char *argv[])
{

  delta (argv[1]);

}

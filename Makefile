
all: read csv entropy delta entropy_bs bitpack p4 hist range huff size print_hist opt_size unique write hist2 histo div

clean:
	rm -f read csv entropy entropy_bs delta bitpack hist p4 range huff ubp bp size print_hist opt_size unique hist2 histo write div

s:histo hist

read: read.c
	gcc read.c -o read
csv: csv.c
	gcc csv.c -o csv
entropy:entropy.cc
	g++ entropy.cc -o entropy  -std=c++11
entropy_bs:entropy_bs.c
	gcc entropy_bs.c -o entropy_bs -lm
delta:delta.c
	gcc delta.c -o delta
bitpack:bitpack.cc
	g++ bitpack.cc -o bp
	g++ bitpack.cc -o ubp
hist:hist.cc
	g++ hist.cc -o hist
p4:p4.cc
	g++ p4.cc -o p4
hist2:hist2.cc
	g++ hist2.cc -o hist2
write:write.c
	gcc write.c -o write
range:range.cc
	g++ range.cc -o range

huff:huffman.c huffcode.c huffman.h opt.h getopt.c 
	gcc huffman.c huffcode.c getopt.c -o huff
size:size.c
	gcc size.c -o size
print_hist:print_hist.cc
	g++ print_hist.cc -o print_hist	

opt_size:opt_size.cc
	g++ opt_size.cc -o opt_size
unique:unique.cc
	g++ unique.cc -o unique
histo:hist.cc
	g++ hist.cc -o histv1 -O3
div:divide.c
	gcc divide.c -o div

#ifdef _MSC_VER 
#define _CRT_SECURE_NO_WARNINGS
#else 
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <limits.h>
#include <sys/stat.h>
long
	GetFileSize (char *filename)
{
	struct stat stat_buf;
	int rc = stat (filename, &stat_buf);
	return rc == 0 ? stat_buf.st_size : -1;
}
int main(int argc, char * argv[]){
char buf[1024];
sprintf(buf, "%s.xz",argv[1]);
long long sz=GetFileSize(buf)*8;
FILE *ff = fopen ("results", "a");
	
	fprintf (ff, "%s\tXZ\t%lu\n", argv[1], sz);
	fclose (ff);
	

}

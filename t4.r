compress <- function(filename, method, level=NA) {
	start_time <- proc.time()
	cfile <- paste0(filename, ".", method, ".", level)
	if (!file.exists(cfile)) {
		cflag <- ""
		if (!is.na(level)) {
			cflag <- paste0("-",level)
		}
	if(method=="xz")	
		system(paste0(method, " -c -k ", cflag, "  ", filename, " > ", cfile))
	else if (method=="gzip")	
	    system(paste0(method, " -c ", cflag, "  ", filename, " > ", cfile))
	else  if (method=="bzip2")
	     system(paste0(method, " -c ", cflag, "  ", filename, " > ", cfile))
	else 
    	system(paste0(method, "  ", filename))
	}
	end_time <- proc.time()
	t <- end_time - start_time
	t <-t["elapsed"]
	size <-file.info(cfile)$size
	data.frame(size,t)		
}

test_ratio <- function(filename) {
 	gzipl=compress(filename, "gzip", 1)
	gzipd=compress(filename, "gzip", NA)
	gziph=compress(filename, "gzip", 9)
	bzl=compress(filename, "bzip2", 1)
	bzd=compress(filename, "bzip2", NA)
	bzh=compress(filename, "bzip2", 9)
	xzl=compress(filename, "xz", 1)
	xzd=compress(filename, "xz", NA)
	xzh=compress(filename, "xz", 9)
	histo=compress(filename, "histo", 9)

	data.frame(filename=filename, original=file.info(filename)$size, 
		gzip_low_time=gzipl$t,
		gzip_dft_time=gzipd$t,
		gzip_hig_time=gziph$t,
		bzip2_low_time=bzl$t,
		bzip2_dft_time=bzd$t,
		bzip2_hig_time=bzh$t,
		xz_low_time=xzl$t,
		xz_dft_time=xzd$t,
		xz_hig_time=xzh$t,
		histo_time=histo$t,
		gzip_low=gzipl$size,		
		gzip_dft=gzipd$size,
		gzip_hig=gziph$size,
		bzip2_low=bzl$size,
		bzip2_dft=bzd$size,
		bzip2_hig=bzh$size,
		xz_low=xzl$size,
		xz_dft=xzd$size,
		xz_hig=xzh$size,
		histos=histo$size)
}


write_bin_int <- function(filename, vector) {
	f <- file(filename, open="wb", raw=T)
	writeBin(as.integer(vector), f, size=4)
	close(f)
}


sds   <- c(0.000000001, 0.00000001, 0.0000001, 0.000001, 0.00001, 0.0001, 0.001, 0.01, 0.1, 1)

library(parallel)
system("mkdir art4")
system("rm -f art4/*z*")
system("rm -f art4/*h*")

rr <- do.call("rbind", mclapply(sds, function(s) {
	        n<-1000*1000*1
		fname <- paste0("art4/s-",s,".bat")
		sd <- (.Machine$integer.max/4)*s
		ds <- as.integer(rnorm(n,sd=sd)) #911630
		write_bin_int(fname, ds)
		ret <- test_ratio(fname)
		ret$s <- s
		ret$sd <- sd
		ret
}))

print(rr)

write.csv(rr, "all4.csv")


	

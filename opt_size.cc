#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#else
#include <unistd.h>
#endif
#include <sys/types.h>
#include <sys/stat.h>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <limits.h>
#include <string.h>
#define STA
#ifdef STA
int *numbers;
int cnt;
#else
extern int *numbers;
extern int cnt;
#endif

using namespace std;

long
GetFileSize (char *filename)
#ifdef 	STA
{
  struct stat stat_buf;
  int rc = stat (filename, &stat_buf);
  return rc == 0 ? stat_buf.st_size : -1;
}
#else
 ;
#endif

     void read (char *filename)
#ifdef 	STA
{
  long sz = GetFileSize (filename);
  numbers = (int *) malloc (sz);
  FILE *f = fopen (filename, "rb");
  fread (numbers, sizeof (int), sz / sizeof (int), f);
  fclose (f);
  cnt = sz / sizeof (int);
}
#else
 ;
#endif

     int compare (const void *a, const void *b)
#ifdef 	STA
{
  return (*(int *) a - *(int *) b);
}
#else
;
#endif

     int unique_count;
     std::vector < int >
       sorted_unique_numbers;
     std::vector < int >
       cnts;

     void
     init ()
{

  qsort (numbers, cnt, sizeof (int), compare);
  unique_count = 0;
  int
    prev = numbers[0] - 1;

  for (int i = 0; i < cnt; i++)
    {
      numbers[i] = numbers[i];
      if (numbers[i] == prev)
	{
	  cnts[unique_count - 1]++;
	}
      else
	{
	  unique_count++;
	  cnts.push_back (1);
	  sorted_unique_numbers.push_back (numbers[i]);
	}
      prev = numbers[i];
    }
}

int
encode_len (int x)
{

  if (x == 0)
    return 1;
  else if (x < 0)
    x = -x;
  return ceil (log (x) / log (2)) + 1;
}

int
main (int argc, char *argv[])
{
  char *
    filename = argv[1];
  read (filename);
  for (int i = 0; i < cnt; i++)
    printf ("%d\t%d\n", numbers[i], encode_len (numbers[i]));
  init ();
  long long
    size = 0;
  for (int i = 0; i < sorted_unique_numbers.size (); i++)
    {
      int
	n = sorted_unique_numbers[i];
      if (n < 0)
	n = -n;
      if (n == 0)
	continue;
      size += (cnts[i] * (1 + log (n) / log (2)));
    }

  printf ("Size %lu \n", size);
  return 0;

}

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#else
#include <unistd.h>
#endif
#define TOO_VERBOSE
#include <sys/types.h>
#include <sys/stat.h>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <limits.h>
#include <string.h>

using namespace std;

int *numbers;
int cnt;
long header_sz = 0;
size_t nlogb = 0;
size_t nother = 0;
long buckets_n = 0;
int unique_count;
std::vector <int>	sorted_unique_numbers;
std::vector <int>	cnts;
//start, end and bits fro buckets
vector<int> sts;
vector<int> ends;
vector<int> bits;

long GetFileSize(char *filename)
{
	struct stat stat_buf;
	int rc = stat(filename, &stat_buf);
	return rc == 0 ? stat_buf.st_size : -1;
}

void read(char *filename)

{
	long sz = GetFileSize(filename);
	numbers = (int *)malloc(sz);
	FILE *f = fopen(filename, "rb");
	fread(numbers, sizeof(int), sz / sizeof(int), f);
	fclose(f);
	cnt = sz / sizeof(int);
}

int compare(const void *a, const void *b)
{
	return (*(int *)a - *(int *)b);
}


void 	init()
{
	qsort(numbers, cnt, sizeof(int), compare);
	unique_count = 0;
	int		prev = numbers[0] - 1;

	for (int i = 0; i < cnt; i++)
	{
		if (numbers[i] == prev)
		{
			cnts[unique_count - 1]++;
		}
		else
		{
			unique_count++;
			cnts.push_back(1);
			sorted_unique_numbers.push_back(numbers[i]);
		}
		prev = numbers[i];
	}

}

long processL(int bucket_bits)
{
	sts.clear();
	ends.clear();
	bits.clear();

	unsigned long no_of_buckets = 1 << bucket_bits;
	int points_per_bucket = ceil(cnt * 1.0 / no_of_buckets);
	size_t size = 0;
	int	i = 0;			//for uniqe
	for (int j = 0; j < no_of_buckets; j++)
	{
		//add points as long as we still have 
		int	x = 0;
		sts.push_back(i);
		long range = sorted_unique_numbers[i];
		while (x < points_per_bucket && i < unique_count)
		{
			x += cnts[i];
			i++;
		}
		ends.push_back(i >= unique_count ? unique_count - 1 : i);
		range =
			sorted_unique_numbers[i >=	unique_count ? unique_count - 1 : i] - range + 1;
		int		bits_for_bucket = 0;
		if (range > 0)
			bits_for_bucket = ceil(log(range) / log(2));
		bits.push_back(bits_for_bucket);
		//if we are done with all point then break
		size +=
			32 /*size of the offset of the bucket */ +
			5
			/* for the number of bits for this bucket, actually 5 bits is sufficietn */
			+ (bits_for_bucket + bucket_bits) * x;
		header_sz += (32 + 5);
		nother += bits_for_bucket*x;
		nlogb += bucket_bits;
		buckets_n++;
		if (i >= unique_count)
			break;
	}
	return size;
	
}


long optimize(){
	long diff = 0;
	if (sts.size() == 0) return 0;
	for (int i = 0; i < (sts.size() - 1); i++){
		if (bits[i] < bits[i + 1]){
			//move from bucket i+1 to bucket i
			int j = sts[i];
			long end = sorted_unique_numbers[j] + (1 << bits[i]) - 1;
			j = sts[i + 1];
			long x = 0;
			while (sorted_unique_numbers[j] <end){
				x += cnts[j];
				j++;
				if (j>unique_count) break;
			}
			int o = ends[i];
			ends[i] = j;
			sts[i + 1] = j;
			diff += (bits[i + 1] - bits[i])*x;
		}
		else
			if (bits[i] > bits[i + 1]){
				//move from buckt i+1 to i
			}
	}
	return diff;
}

#define BL 8

void bitpack(char *target, size_t * target_offset, int encoding_length, int source)
{
	int j;
	for (j = encoding_length - 1; j >= 0; j--, (*target_offset)++)
	{
		target[(*target_offset) / BL] |=
			((source >> j) & 1) << ((BL - (*target_offset) % BL) - 1);
	}
}

int bitunpack(char *source, size_t * source_offset, int encoding_length)
{
	int	j, target = 0;
	for (j = encoding_length - 1; j >= 0; j--, (*source_offset)++)
	{
		target |= (1 &
			(source[(*source_offset) / BL] >>
			((BL - (*source_offset) % BL) - 1))) << j;
	}
	return target;
}

#if 0
int main(int argc, char *argv[])
{
	int		increasing = 0;
	long		prev = ULLONG_MAX;

#ifdef _MSC_VER
	char *		filename = "c:/data/art2/art2-s1e-04.bat";
#else
	char *
		filename = argv[1];
#endif
	read(filename);
	init();


#ifdef TOO_VERBOSE
	FILE *		f = fopen("hist_details", "a");
#endif
	for (int i = 0; i <= 32; i++)
	{
		long	p = processL(i);
		p -= optimize();
#ifdef TOO_VERBOSE
		//fprintf(f, "%s\tHL\t%lu\t@%d\n", filename, p, i);
		fprintf(f, "%s\t%d\t%d\t%lu\t%lu\t%lu\t%lu\n", filename, i, buckets_n, header_sz, nlogb, nother, p);
#endif
		header_sz = 0;
		nlogb = 0;
		nother = 0;
		buckets_n = 0;
	}

#ifdef TOO_VERBOSE
	fclose(f);
#endif
	free(numbers);

	return 0;
}
#endif



int
	main (int argc, char *argv[])
{
	int
		increasing = 0;
	long
		prev = ULLONG_MAX;

#ifdef _MSC_VER
	char *
		filename = "c:/data/art2-random-delta.bat";
#else
	char *
		filename = argv[1];
#endif
	read (filename);
	init ();
	unsigned long
		min = ULLONG_MAX;
	int
		min_i = -1;
#ifdef TOO_VERBOSE
	FILE *
		f = fopen ("hist_details", "a");
#endif
	for (int i = 1; i < 31; i++)
	{
		long
			p = processL (i);
		//p-=optimize();
#ifdef TOO_VERBOSE
		fprintf (f, "%s\tHL\t%lu\t@%d\n", filename, p, i);
#endif
		if (min > p)
		{
			min = p;
			min_i = i;
		}

		if (prev < p && increasing)
		{
			break;
		}
		if (prev < p)
		{
			increasing = 1;
		}
		else
			increasing = 0;
		prev = p;

	}
	
#ifdef TOO_VERBOSE
	fclose (f);
#endif
	free (numbers);
	read (filename);
	long
		smin = min;
	//compress (cnt *4, min_i, "out");
	compress(min_i,min/8,"out");
	long	sz = GetFileSize ("out");
	FILE *		ff = fopen ("results", "a");
	fprintf (ff, "%s\tHC\t%lu\t%lu\t@%d\n", filename, smin, sz * 8, min_i);
	fprintf (stdout, "%s\tHC\t%lu\t%lu\t@%d\n", filename, smin, sz * 8, min_i);

	fclose (ff);
	return 0;
}

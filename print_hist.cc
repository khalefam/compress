#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#else
#include <unistd.h>
#endif
#include <sys/types.h>
#include <sys/stat.h>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <vector>
#include <limits.h>
#include <string.h>
#define STA
#ifdef STA
int *numbers;
int cnt;
#else
extern int *numbers;
extern int cnt;
#endif

using namespace std;

long
GetFileSize (char *filename)
#ifdef 	STA
{
  struct stat stat_buf;
  int rc = stat (filename, &stat_buf);
  return rc == 0 ? stat_buf.st_size : -1;
}
#else
 ;
#endif

     void read (char *filename)
#ifdef 	STA
{
  long sz = GetFileSize (filename);
  numbers = (int *) malloc (sz);
  FILE *f = fopen (filename, "rb");
  fread (numbers, sizeof (int), sz / sizeof (int), f);
  fclose (f);
  cnt = sz / sizeof (int);
}
#else
 ;
#endif

     int compare (const void *a, const void *b)
#ifdef 	STA
{
  return (*(int *) a - *(int *) b);
}
#else
;
#endif

     int unique_count;
     std::vector < int >
       sorted_unique_numbers;
     std::vector < int >
       cnts;
#define W 1
     void
     init ()
{

  qsort (numbers, cnt, sizeof (int), compare);
  unique_count = 0;
  int
    prev = numbers[0] / W - 1;

  for (int i = 0; i < cnt; i++)
    {
      numbers[i] = numbers[i] / W;
      if (numbers[i] == prev)
	{
	  cnts[unique_count - 1]++;
	}
      else
	{
	  unique_count++;
	  cnts.push_back (1);
	  sorted_unique_numbers.push_back (numbers[i]);
	}
      prev = numbers[i];
    }

}

int
encode_len (int x)
{

  if (x == 0)
    return 1;
  else if (x < 0)
    x = -x;
  return ceil (log (x) / log (2)) + 1;
}

int
main (int argc, char *argv[])
{
  //writerandomfile("c:/data/t.bat");
  //      return 1;
#ifdef _MSC_VER
  //char *filename="c:/data/t.bat";
  //char *filename="c:/data/lofar.tsv-2.bat";
  char *
    filename = "c:/data/art2-s0.001.bat";
#else
  char *
    filename = argv[1];
#endif
  read (filename);
  init ();
  for (int i = 0; i < sorted_unique_numbers.size (); i++)
    {
      printf ("%d %d %d\n", sorted_unique_numbers[i], cnts[i],
	      encode_len (sorted_unique_numbers[i]));
    }
  return 0;

}

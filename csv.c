#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char *
getfield (char *line, int num)
{
  const char *tok;
  for (tok = strtok (line, ",;\t");
       tok && *tok; tok = strtok (NULL, ",\t;\n"))
    {
      if (!--num)
	return tok;
    }
  return NULL;
}

int
main (int argc, char *argv[])
{
  FILE *stream = fopen (argv[1], "r");
  char outfile[1024];
  sprintf (outfile, "%s-%s.bat", argv[1], argv[2]);
  FILE *outf = fopen (outfile, "w");
  int c = atoi (argv[2]);

  char line[1024];
  int numbers[1024];
  int n = 0;
  while (fgets (line, 1024, stream))
    {
      char *tmp = strdup (line);
      
      numbers[n++] = atof (getfield(tmp,c)) * 100 * 100 * 10;
      if (n >= 1000)
	{
	  fwrite (numbers, sizeof (int), n, outf);
	  n = 0;
	}
      free (tmp);
    }
  if (n > 0)
    fwrite (numbers, sizeof (int), n, outf);

  fclose (stream);
  fclose (outf);
}
